package ru.itis.karakurik.tinkofflab.data.network.mapper

import ru.itis.karakurik.tinkofflab.data.network.response.currencies.CurrenciesRateResponse
import ru.itis.karakurik.tinkofflab.domain.entity.Currency

fun CurrenciesRateResponse.mapToCurrenciesList() =
    rates?.entrySet()?.map { currency ->
        currency.value.asJsonObject.let {
            Currency(
                ticker = currency.key.toString(),
                base = base,
                startRate = it.get("start_rate").asDouble,
                endRate = it.get("end_rate").asDouble,
                change = it.get("change").asDouble,
                changePct = it.get("change_pct").asDouble * 100
            )
        }
    }.orEmpty()
