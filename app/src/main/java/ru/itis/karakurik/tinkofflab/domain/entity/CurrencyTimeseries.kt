package ru.itis.karakurik.tinkofflab.domain.entity

data class CurrencyTimeseries(
    val ticker: String,
    val base: String
)
