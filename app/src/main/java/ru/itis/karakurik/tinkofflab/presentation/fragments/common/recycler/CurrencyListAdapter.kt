package ru.itis.karakurik.tinkofflab.presentation.fragments.common.recycler

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import ru.itis.karakurik.tinkofflab.domain.entity.Currency

class CurrencyListAdapter(
    private val changeFavourite: (currency: Currency) -> Unit,
    private val onItemClick: (ticker: String) -> Unit
) : ListAdapter<Currency, CurrencyViewHolder>(DiffCurrencyCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = CurrencyViewHolder.create(
        parent,
        changeFavourite,
        onItemClick
    )

    override fun onBindViewHolder(
        holder: CurrencyViewHolder,
        position: Int
    ) {
        holder.bind(getItem(position))
    }
}
