package ru.itis.karakurik.tinkofflab.presentation.fragments.market

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.itis.androidlabproject.decorator.SpaceItemDecorator
import dagger.hilt.android.AndroidEntryPoint
import ru.itis.karakurik.tinkofflab.databinding.FragmentMarketBinding
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.usecase.GetCurrenciesRateFromLocaleUseCase
import ru.itis.karakurik.tinkofflab.domain.usecase.GetCurrenciesRateFromRemoteUseCase
import ru.itis.karakurik.tinkofflab.domain.usecase.GetSupportedSymbolsUseCase
import ru.itis.karakurik.tinkofflab.domain.utils.Status
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toast
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.recycler.CurrencyListAdapter
import ru.itis.karakurik.tinkofflab.presentation.fragments.favorities.FavoritesViewModel
import ru.itis.karakurik.tinkofflab.presentation.fragments.viewPager2.ViewPager2FragmentDirections
import javax.inject.Inject

@AndroidEntryPoint
class MarketFragment : Fragment() {

    private var _binding: FragmentMarketBinding? = null
    private val binding get() = _binding!!
    private var currencyListAdapter: CurrencyListAdapter? = null
    private var favouriteCurrencies: List<Currency> = listOf()
    private var currencies: List<Currency> = listOf()

    private val viewModel: MarketViewModel by viewModels()
    private val favouritesViewModel: FavoritesViewModel by viewModels()

    @Inject
    lateinit var getCurrenciesRateFromRemoteUseCase: GetCurrenciesRateFromRemoteUseCase

    @Inject
    lateinit var getCurrenciesRateFromLocaleUseCase: GetCurrenciesRateFromLocaleUseCase

    @Inject
    lateinit var getSupportedSymbolsUseCase: GetSupportedSymbolsUseCase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMarketBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initSwipeRefreshLayout()
        initCollectFlows()
        initSearchView()
        showCurrencies()
    }

    private fun initSearchView() {
        binding.svSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                updateCurrencies(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                updateCurrencies(newText)
                return false
            }
        })
    }

    private fun initCollectFlows() {
        lifecycleScope.launchWhenStarted {
            favouritesViewModel.favouriteCurrencies.collect { state ->
                when (state.status) {
                    Status.SUCCESS -> {
                        favouriteCurrencies = state.data.orEmpty()
                        updateCurrencies()
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.currenciesRate.collect { state ->
                binding.swipeRefreshLayout.isRefreshing = false
                when (state.status) {
                    Status.SUCCESS -> {
                        currencies = state.data.orEmpty()
                        updateCurrencies()
                    }
                    Status.ERROR -> {
                        requireContext().toast(state.message)
                        viewModel.getCurrenciesRateFromLocale()
                    }
                    Status.LOADING -> {
                        binding.swipeRefreshLayout.isRefreshing = true
                    }
                }
            }
        }
    }

    private fun updateCurrencies(text: String? = null) {
        currencies.filter {
            it.base != it.ticker
        }.also {
            text?.let { text ->
                it.filter {
                    it.ticker.lowercase().contains(text.lowercase())
                }
            }
        }.onEach { currency ->
            favouriteCurrencies.onEach { favouriteCurrency ->
                if (currency.ticker == favouriteCurrency.ticker) {
                    currency.isFavourite = favouriteCurrency.isFavourite
                }
            }
        }.also {
            currencyListAdapter?.submitList(it.toList())
            binding.rvList.layoutManager?.scrollToPosition(0)
        }
    }

    private fun initRecyclerView() {
        currencyListAdapter = CurrencyListAdapter(
            changeFavourite = {
                if (it.isFavourite) {
                    viewModel.addCurrencyToFavorites(it)
                } else {
                    viewModel.deleteCurrencyFromFavorites(it)
                }
                favouritesViewModel.getFavouriteCurrencies()
            },
            onItemClick = {
                showDetailsFragment(it)
            }
        )

        binding.rvList.run {
            adapter = currencyListAdapter
            addItemDecoration(SpaceItemDecorator(requireContext()))
        }
    }

    private fun showDetailsFragment(
        ticker: String,
    ) = findNavController().navigate(
        ViewPager2FragmentDirections.actionViewPager2FragmentToDetailsFragment(
            ticker
        )
    )

    private fun initSwipeRefreshLayout() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            showCurrencies()
        }
    }

    private fun showCurrencies() {
        favouritesViewModel.getFavouriteCurrencies()
        viewModel.getCurrenciesRateFromRemote(null, null)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        currencyListAdapter = null
    }
}
