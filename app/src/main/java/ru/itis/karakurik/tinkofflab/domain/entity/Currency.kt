package ru.itis.karakurik.tinkofflab.domain.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency")
data class Currency(
    @PrimaryKey
    @ColumnInfo(name = "ticker")
    val ticker: String,

    @ColumnInfo(name = "base")
    var base: String? = "USD",

    @ColumnInfo(name = "start_rate")
    val startRate: Double,

    @ColumnInfo(name = "start_rate_inverse")
    val startRateInverse: Double = 1.0 / startRate,

    @ColumnInfo(name = "end_rate")
    val endRate: Double,

    @ColumnInfo(name = "end_rate_inverse")
    val endRateInverse: Double = 1.0 / endRate,

    @ColumnInfo(name = "change")
    val change: Double,

    @ColumnInfo(name = "change_inverse")
    val changeInverse: Double = endRateInverse - startRateInverse,

    @ColumnInfo(name = "change_pct")
    val changePct: Double,

    @ColumnInfo(name = "change_pct_inverse")
    val changePctInverse: Double = changeInverse / startRateInverse * 100,

    @ColumnInfo(name = "is_favourite")
    var isFavourite: Boolean = false
)
