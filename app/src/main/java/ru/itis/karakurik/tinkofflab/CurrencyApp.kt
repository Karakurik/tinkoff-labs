package ru.itis.karakurik.tinkofflab

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CurrencyApp: Application()
