package ru.itis.karakurik.tinkofflab.data.network.response.currencies


import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class CurrenciesRateResponse(
    @SerializedName("motd")
    val motd: Motd?,
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("fluctuation")
    val fluctuation: Boolean?,
    @SerializedName("timeseries")
    val timeseries: Boolean?,
    @SerializedName("base")
    var base: String?,
    @SerializedName("start_date")
    val startDate: String?,
    @SerializedName("end_date")
    val endDate: String?,
    @SerializedName("rates")
    val rates: JsonObject?
) {
    data class Motd(
        @SerializedName("msg")
        val msg: String?,
        @SerializedName("url")
        val url: String?
    )
}
