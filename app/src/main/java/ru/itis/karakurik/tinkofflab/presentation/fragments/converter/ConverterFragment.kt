package ru.itis.karakurik.tinkofflab.presentation.fragments.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import ru.itis.karakurik.tinkofflab.R
import ru.itis.karakurik.tinkofflab.databinding.FragmentConverterBinding
import ru.itis.karakurik.tinkofflab.domain.utils.Status
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toInvisible
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toVisible
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toast

@AndroidEntryPoint
class ConverterFragment : Fragment() {
    private var _binding: FragmentConverterBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ConverterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConverterBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getSupportedCurrencies()

        initListeners()
        collectFlows()
    }

    private fun initListeners() {
        with(binding) {
            btnConvert.setOnClickListener {
                kotlin.runCatching {
                    etAmount.text.toString().toDouble()
                }.onSuccess {
                    viewModel.convert(
                        spinnerBase.selectedItem.toString(),
                        spinnerTicker.selectedItem.toString(),
                        it
                    )
                }.onFailure {
                    requireContext().toast(getString(R.string.invalid_number))
                }
            }
            swipeRefreshLayout.setOnRefreshListener {
                viewModel.getSupportedCurrencies()
                swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun collectFlows() {
        lifecycleScope.launchWhenStarted {
            viewModel.supportedCurrencies.collect { state ->
                when (state.status) {
                    Status.LOADING -> showProgressBar()
                    Status.ERROR -> {
                        hideProgressBar()
                        requireContext().toast(state.message)
                    }
                    Status.SUCCESS -> {
                        hideProgressBar()
                        if (state.data.isNullOrEmpty()) {
                            requireContext().toast("Не удалось получить список доступных валют")
                        } else {
                            initializeSpinner(state.data.map {
                                it.ticker
                            }.toList())
                        }
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.convertStateFlow.collect { state ->
                when (state.status) {
                    Status.LOADING -> {
                        showProgressBar()
                    }
                    Status.SUCCESS -> {
                        hideProgressBar()
                        binding.tvResult.text = getString(
                            R.string.double_number_2,
                            state.data
                        )
                    }
                    Status.ERROR -> {
                        hideProgressBar()
                        requireContext().toast(state.message)
                    }
                }
            }
        }
    }

    private fun initializeSpinner(supportedCurrencies: List<String>) {
        val spinnerAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            supportedCurrencies
        )

        with(binding) {
            spinnerBase.run {
                adapter = spinnerAdapter
                setSelection(supportedCurrencies.indexOfFirst {
                    it == "USD"
                }, false)
            }
            spinnerTicker.run {
                adapter = spinnerAdapter
                setSelection(supportedCurrencies.indexOfFirst {
                    it == "RUB"
                }, false)
            }
        }
    }

    private fun hideProgressBar() {
        binding.progressBar.toInvisible()
    }

    private fun showProgressBar() {
        binding.progressBar.toVisible()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
