package ru.itis.karakurik.tinkofflab.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.data.database.CurrencyDao
import ru.itis.karakurik.tinkofflab.di.module.AppModule
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.repository.MarketRepository
import javax.inject.Inject

class GetCurrenciesRateFromRemoteUseCase @Inject constructor(
    private val marketRepository: MarketRepository,
    private val currencyDao: CurrencyDao,
    @AppModule.IODispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) {
    suspend operator fun invoke(
        startDate: String?,
        endDate: String?,
        base: String = "USD"
    ): List<Currency> {
        return withContext(dispatcher) {
            marketRepository.getCurrenciesRateFromRemote(
                base,
                startDate,
                endDate
            )
        }
    }
}
