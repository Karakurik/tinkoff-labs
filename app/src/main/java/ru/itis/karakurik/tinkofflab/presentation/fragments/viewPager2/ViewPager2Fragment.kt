package ru.itis.karakurik.tinkofflab.presentation.fragments.viewPager2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.tabs.TabLayoutMediator
import ru.itis.karakurik.tinkofflab.R
import ru.itis.karakurik.tinkofflab.databinding.FragmentViewPager2Binding
import ru.itis.karakurik.tinkofflab.presentation.fragments.converter.ConverterFragment
import ru.itis.karakurik.tinkofflab.presentation.fragments.favorities.FavoritesFragment
import ru.itis.karakurik.tinkofflab.presentation.fragments.favorities.FavoritesViewModel
import ru.itis.karakurik.tinkofflab.presentation.fragments.market.MarketFragment

class ViewPager2Fragment : Fragment() {
    private var _binding: FragmentViewPager2Binding? = null
    private val binding get() = _binding!!

//    private val viewModel: FavoritesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentViewPager2Binding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)

        val list = arrayListOf(
            ConverterFragment(),
            MarketFragment(),
            FavoritesFragment()
        )

        val adapter = ViewPager2Adapter(
            list,
            childFragmentManager,
            lifecycle
        )

        with(binding) {
            vp2.adapter = adapter

            TabLayoutMediator(tabLayout, vp2) { tab, position ->
                tab.text = when (position) {
                    0 -> getString(R.string.converter)
                    1 -> getString(R.string.markets)
                    else -> getString(R.string.favorites)
                }
            }.attach()
//            vp2[2].isVisible = false
        }

//        viewModel.getFavouriteCurrencies()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
