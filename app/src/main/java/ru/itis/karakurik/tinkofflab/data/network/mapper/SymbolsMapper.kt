package ru.itis.karakurik.tinkofflab.data.network.mapper

import com.google.gson.Gson
import ru.itis.karakurik.tinkofflab.data.network.response.symbols.SupportedSymbol
import ru.itis.karakurik.tinkofflab.data.network.response.symbols.SupportedSymbolsResponse
import ru.itis.karakurik.tinkofflab.domain.entity.SupportedCurrency

fun SupportedSymbolsResponse.mapToSupportedCurrencyList() =
    symbols?.entrySet()?.map { symbol ->
        symbol.value.asJsonObject.let {
            Gson().fromJson(it, SupportedSymbol::class.java).mapToSupportedCurrency()
        }
    }.orEmpty()

fun SupportedSymbol.mapToSupportedCurrency() = SupportedCurrency(
    ticker = code,
    description = description
)
