package ru.itis.karakurik.tinkofflab.domain.entity

data class SupportedCurrency(
    val ticker: String,
    val description: String
)
