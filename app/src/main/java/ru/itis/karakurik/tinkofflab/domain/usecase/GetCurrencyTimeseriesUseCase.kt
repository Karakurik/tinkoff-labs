package ru.itis.karakurik.tinkofflab.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.di.module.AppModule
import ru.itis.karakurik.tinkofflab.domain.entity.CurrencyGraphEntity
import ru.itis.karakurik.tinkofflab.domain.repository.MarketRepository
import javax.inject.Inject

class GetCurrencyTimeseriesUseCase @Inject constructor(
    private val marketRepository: MarketRepository,
    @AppModule.IODispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) {

    suspend operator fun invoke(
        base: String,
        currency: String,
        startDate: String,
        endDate: String
    ): Array<CurrencyGraphEntity>? {
        return withContext(dispatcher) {
            marketRepository.getCurrencyTimeseries(
                base,
                currency,
                startDate,
                endDate
            )
        }
    }
}
