package ru.itis.karakurik.tinkofflab.presentation.fragments.currencyDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.jjoe64.graphview.series.LineGraphSeries
import dagger.hilt.android.AndroidEntryPoint
import ru.itis.karakurik.tinkofflab.R
import ru.itis.karakurik.tinkofflab.databinding.FragmentDetailsBinding
import ru.itis.karakurik.tinkofflab.domain.utils.DateHelper
import ru.itis.karakurik.tinkofflab.domain.utils.Status
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toast

@AndroidEntryPoint
class DetailsFragment : Fragment() {
    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!

    private val viewModel: DetailsViewModel by viewModels()

    private val args: DetailsFragmentArgs by navArgs()

    private var chosenDate: String = DateHelper.monthsAgo()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        showGraph()
        initListeners()
        initCollectFlows()
        initSwipeRefreshLayout()
    }

    private fun initSwipeRefreshLayout() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            showGraph()
        }
    }

    private fun initCollectFlows() {
        lifecycleScope.launchWhenStarted {
            viewModel.currencyTimeseries.collect { state ->
                binding.swipeRefreshLayout.isRefreshing = false
                when (state.status) {
                    Status.SUCCESS -> {
                        binding.graphView.run {
                            removeAllSeries()
                            addSeries(
                                LineGraphSeries(state.data)
                            )
                        }
                    }
                    Status.ERROR -> requireContext().toast("Не удалось обновить график")
                    Status.LOADING -> {
                        binding.swipeRefreshLayout.isRefreshing = true
                    }
                }
            }
        }
    }

    private fun initListeners() {
        with(binding) {
            btn3years.setOnClickListener {
                chosenDate = DateHelper.yearsAgo(3)
                showGraph(chosenDate)
                tvFreq.text = "3 Года"
            }
            btn1year.setOnClickListener {
                chosenDate = DateHelper.yearsAgo()
                showGraph(chosenDate)
                tvFreq.text = "Год"
            }
            btnMonth.setOnClickListener {
                chosenDate = DateHelper.monthsAgo()
                showGraph(chosenDate)
                tvFreq.text = "Месяц"
            }
            btnWeek.setOnClickListener {
                chosenDate = DateHelper.weeksAgo()
                showGraph(chosenDate)
                tvFreq.text = "Неделя"
            }
        }
    }

    private fun showGraph(
        base: String = getString(R.string.base_currency_ticker),
        ticker: String = args.ticker,
        startDate: String = chosenDate,
        endDate: String = DateHelper.today()
    ) {
        viewModel.getCurrencyTimeseries(
            base, ticker, startDate, endDate
        )
    }
}
