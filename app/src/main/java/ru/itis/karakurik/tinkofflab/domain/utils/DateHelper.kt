package ru.itis.karakurik.tinkofflab.domain.utils

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DateHelper {

    companion object {
        private const val pattern: String = "yyyy-MM-dd"
        private val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern(pattern)

        fun today(): String = LocalDate.now().format(dtf)

        fun daysAgo(cou: Long = 1): String = LocalDate.now().minusDays(cou).format(dtf)

        fun weeksAgo(cou: Long = 1): String = LocalDate.now().minusWeeks(cou).format(dtf)

        fun monthsAgo(cou: Long = 1): String = LocalDate.now().minusMonths(cou).format(dtf)

        fun yearsAgo(cou: Long = 1): String = LocalDate.now().minusYears(cou).format(dtf)

        fun toDouble(strDate: String): Double =
            LocalDate.parse(strDate, dtf).toEpochDay().toDouble()
    }
}
