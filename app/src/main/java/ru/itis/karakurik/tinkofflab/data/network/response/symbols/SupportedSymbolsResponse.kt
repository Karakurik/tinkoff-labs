package ru.itis.karakurik.tinkofflab.data.network.response.symbols


import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class SupportedSymbolsResponse(
    @SerializedName("motd")
    val motd: Motd?,
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("symbols")
    val symbols: JsonObject?
) {
    data class Motd(
        @SerializedName("msg")
        val msg: String?,
        @SerializedName("url")
        val url: String?
    )
}
