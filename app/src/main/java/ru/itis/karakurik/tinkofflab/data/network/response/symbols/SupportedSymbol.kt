package ru.itis.karakurik.tinkofflab.data.network.response.symbols

import com.google.gson.annotations.SerializedName

data class SupportedSymbol(
    @SerializedName("code")
    val code: String,
    @SerializedName("description")
    val description: String
)
