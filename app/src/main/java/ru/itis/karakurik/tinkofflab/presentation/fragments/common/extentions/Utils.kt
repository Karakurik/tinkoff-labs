package ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions

import android.content.Context
import android.widget.Toast

internal fun Context.toast(message: String?, duration: Int = Toast.LENGTH_LONG) =
    message?.let {
        Toast.makeText(
            this,
            message,
            duration
        ).show()
    }
