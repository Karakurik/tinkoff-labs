package ru.itis.karakurik.tinkofflab.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.di.module.AppModule
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.repository.MarketRepository
import javax.inject.Inject

class GetCurrenciesRateFromLocaleUseCase @Inject constructor(
    private val marketRepository: MarketRepository,
    @AppModule.IODispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) {

    suspend operator fun invoke(base: String = "USD"): List<Currency> {
        return withContext(dispatcher) {
            marketRepository.getCurrenciesRateFromLocal(base)
        }
    }
}
