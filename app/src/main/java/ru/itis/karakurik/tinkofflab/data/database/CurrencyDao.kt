package ru.itis.karakurik.tinkofflab.data.database

import androidx.room.*
import ru.itis.karakurik.tinkofflab.domain.entity.Currency

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currency")
    fun getAllCurrencies(): List<Currency>

    @Query("SELECT * FROM currency WHERE is_favourite=1")
    fun getAllFavouriteCurrencies(): List<Currency>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavoriteCurrency(currency: Currency)

    @Update
    fun updateCurrency(currency: Currency)
}