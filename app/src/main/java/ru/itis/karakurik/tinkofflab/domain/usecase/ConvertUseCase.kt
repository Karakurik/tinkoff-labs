package ru.itis.karakurik.tinkofflab.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.di.module.AppModule
import ru.itis.karakurik.tinkofflab.domain.repository.MarketRepository
import javax.inject.Inject

class ConvertUseCase @Inject constructor(
    private val marketRepository: MarketRepository,
    @AppModule.IODispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) {

    suspend operator fun invoke(
        from: String,
        to: String,
        amount: Double
    ): Double? {
        return withContext(dispatcher) {
            marketRepository.convert(from, to, amount)
        }
    }
}
