package ru.itis.karakurik.tinkofflab.data.network.response.convert


import com.google.gson.annotations.SerializedName

data class ConvertResponse(
    @SerializedName("motd")
    val motd: Motd?,
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("query")
    val query: Query?,
    @SerializedName("info")
    val info: Info?,
    @SerializedName("historical")
    val historical: Boolean?,
    @SerializedName("date")
    val date: String?,
    @SerializedName("result")
    val result: Double?
) {
    data class Motd(
        @SerializedName("msg")
        val msg: String?,
        @SerializedName("url")
        val url: String?
    )

    data class Query(
        @SerializedName("from")
        val from: String?,
        @SerializedName("to")
        val to: String?,
        @SerializedName("amount")
        val amount: Double?
    )

    data class Info(
        @SerializedName("rate")
        val rate: Double?
    )
}
