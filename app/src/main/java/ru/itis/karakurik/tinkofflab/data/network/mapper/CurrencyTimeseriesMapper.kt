package ru.itis.karakurik.tinkofflab.data.network.mapper

import ru.itis.karakurik.tinkofflab.data.network.response.currencies.CurrenciesRateResponse
import ru.itis.karakurik.tinkofflab.domain.entity.CurrencyGraphEntity

fun CurrenciesRateResponse.mapToCurrencyGraphEntityArray() = rates?.entrySet()?.map { date ->
    date.value.asJsonObject.entrySet().first().let { currency ->
        CurrencyGraphEntity(
            ticker = currency.key,
            base = base,
            rate = currency.value.asDouble,
            date = date.key
        )
    }
}?.toTypedArray()
