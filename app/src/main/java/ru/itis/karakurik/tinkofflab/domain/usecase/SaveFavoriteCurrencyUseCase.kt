package ru.itis.karakurik.tinkofflab.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.data.database.CurrencyDao
import ru.itis.karakurik.tinkofflab.di.module.AppModule
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import javax.inject.Inject

class SaveFavoriteCurrencyUseCase @Inject constructor(
    private val currencyDao: CurrencyDao,
    @AppModule.IODispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) {

    suspend operator fun invoke(
        currency: Currency
    ) {
        withContext(dispatcher) {
            currency.isFavourite = true
            currencyDao.insertFavoriteCurrency(currency)
        }
    }
}