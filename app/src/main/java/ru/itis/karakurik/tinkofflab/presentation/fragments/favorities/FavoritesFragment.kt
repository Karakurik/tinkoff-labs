package ru.itis.karakurik.tinkofflab.presentation.fragments.favorities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.itis.androidlabproject.decorator.SpaceItemDecorator
import dagger.hilt.android.AndroidEntryPoint
import ru.itis.karakurik.tinkofflab.databinding.FragmentFavoritesBinding
import ru.itis.karakurik.tinkofflab.domain.utils.Status
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toGone
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toVisible
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.extentions.toast
import ru.itis.karakurik.tinkofflab.presentation.fragments.common.recycler.CurrencyListAdapter
import ru.itis.karakurik.tinkofflab.presentation.fragments.viewPager2.ViewPager2FragmentDirections

@AndroidEntryPoint
class FavoritesFragment : Fragment() {
    private var _binding: FragmentFavoritesBinding? = null
    private val binding get() = _binding!!
    private var currencyListAdapter: CurrencyListAdapter? = null

    private val viewModel: FavoritesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoritesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initCollectFlows()
        initSwipeRefreshLayout()
        showCurrencies()
    }

    private fun initCollectFlows() {
        lifecycleScope.launchWhenStarted {
            viewModel.favouriteCurrencies.collect { state ->
                binding.swipeRefreshLayout.isRefreshing = false
                when (state.status) {
                    Status.SUCCESS -> {
                        state.data?.filter {
                            it.base != it.ticker
                        }.also {
                            binding.rvList.toVisible()
                            currencyListAdapter?.submitList(it)
                            binding.rvList.layoutManager?.scrollToPosition(0)
                            refreshRecyclerView()
                        }
                    }
                    Status.ERROR -> {
                        requireContext().toast(state.message)
                    }
                    Status.LOADING -> {
                        binding.swipeRefreshLayout.isRefreshing = true
                    }
                }
            }
        }
    }

    private fun initRecyclerView() {
        currencyListAdapter = CurrencyListAdapter(
            changeFavourite = {
                currencyListAdapter?.let { adapter ->
                    if (it.isFavourite) {
                        viewModel.addCurrencyToFavorites(it)
                        viewModel.getFavouriteCurrencies()
                    } else {
                        viewModel.deleteCurrencyFromFavorites(it)
                        viewModel.getFavouriteCurrencies()
                    }
                }
            },
            onItemClick = {
                showDetailsFragment(it)
            }
        )
        binding.rvList.run {
            adapter = currencyListAdapter
            addItemDecoration(SpaceItemDecorator(requireContext()))
        }
    }

    private fun showDetailsFragment(
        ticker: String,
    ) = findNavController().navigate(
        ViewPager2FragmentDirections.actionViewPager2FragmentToDetailsFragment(
            ticker
        )
    )

    private fun initSwipeRefreshLayout() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.getFavouriteCurrencies()
        }
    }

    private fun showCurrencies() {
        viewModel.getFavouriteCurrencies()
    }

    private fun refreshRecyclerView() {
        with(binding) {
            if (currencyListAdapter?.currentList.isNullOrEmpty()) {
                rvList.toGone()
//                tvEmpty.toVisible()
            } else {
//                tvEmpty.toGone()
                rvList.toVisible()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
