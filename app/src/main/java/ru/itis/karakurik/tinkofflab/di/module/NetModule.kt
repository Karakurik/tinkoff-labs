package ru.itis.karakurik.tinkofflab.di.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.itis.karakurik.tinkofflab.BuildConfig
import ru.itis.karakurik.tinkofflab.data.network.CurrencyAppApi
import ru.itis.karakurik.tinkofflab.di.qualifier.BaseUrl
import ru.itis.karakurik.tinkofflab.di.qualifier.CacheMaxSize
import ru.itis.karakurik.tinkofflab.di.qualifier.LoggingInterceptor
import java.io.File

private const val BASE_URL = "https://api.exchangerate.host/"

@Module
@InstallIn(SingletonComponent::class)
class NetModule {

    @Provides
    fun provideOkHttpClient(
        cache: Cache,
        @LoggingInterceptor loggingInterceptor: Interceptor,
    ): OkHttpClient = OkHttpClient.Builder()
        .cache(cache)
        .also {
            if (BuildConfig.DEBUG) {
                it.addInterceptor(
                    loggingInterceptor
                )
            }
        }
        .build()

    @Provides
    fun provideApi(
        @BaseUrl baseUrl: String,
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): CurrencyAppApi =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
            .create(CurrencyAppApi::class.java)

    @Provides
    @LoggingInterceptor
    fun provideLoggingInterceptor(): Interceptor =
        HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    fun provideGsonFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @BaseUrl
    fun provideBaseUrl(): String = BASE_URL

    @Provides
    fun provideCacheDirectory(): File {
        return File("cache");
    }

    @CacheMaxSize
    @Provides
    fun provideCacheMaxSize(): Long {
        return 50 * 1024 * 1024;
    }

    @Provides
    fun provideCache(
        cacheDirectory: File,
        @CacheMaxSize cacheMaxSize: Long,
    ): Cache {
        return Cache(cacheDirectory, cacheMaxSize)
    }
}
