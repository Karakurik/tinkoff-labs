package ru.itis.karakurik.tinkofflab.presentation.fragments.market

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.usecase.*
import ru.itis.karakurik.tinkofflab.domain.utils.State
import javax.inject.Inject

@HiltViewModel
class MarketViewModel @Inject constructor(
    private val getCurrenciesRateFromRemoteUseCase: GetCurrenciesRateFromRemoteUseCase,
    private val getCurrenciesRateFromLocaleUseCase: GetCurrenciesRateFromLocaleUseCase,
    private val getFavoritesCurrenciesUseCase: GetFavoritesCurrenciesUseCase,
    private val saveFavoriteCurrencyUseCase: SaveFavoriteCurrencyUseCase,
    private val updateFavoriteCurrencyUseCase: UpdateFavoriteCurrencyUseCase
) : ViewModel() {

    private var _currenciesRate: MutableSharedFlow<State<List<Currency>>> = MutableSharedFlow()
    val currenciesRate: SharedFlow<State<List<Currency>>> = _currenciesRate.asSharedFlow()

    fun getCurrenciesRateFromRemote(
        startDate: String?,
        endDate: String?,
        base: String = "USD"
    ) {
        viewModelScope.launch {
            kotlin.runCatching {
                getCurrenciesRateFromRemoteUseCase(startDate, endDate, base)
            }.onSuccess {
                _currenciesRate.emit(
                    if (it.isEmpty()) {
                        State.error("Не удалось загрузить данные из сети")
                    } else {
                        State.success(it)
                    }
                )
            }.onFailure {
                _currenciesRate.emit(
                    State.error("Не удалось загрузить данные из сети")
                )
            }
        }
    }

    fun getCurrenciesRateFromLocale(
        base: String = "USD"
    ) {
        viewModelScope.launch {
            _currenciesRate.emit(
                getCurrenciesRateFromLocaleUseCase(base).let {
                    if (it.isEmpty()) {
                        State.error("Не удалось загрузить данные из базы данных")
                    } else {
                        State.success(it)
                    }
                }
            )
        }
    }

    fun addCurrencyToFavorites(
        currency: Currency
    ) {
        viewModelScope.launch {
            saveFavoriteCurrencyUseCase(currency)
        }
    }

    fun deleteCurrencyFromFavorites(
        currency: Currency
    ) {
        viewModelScope.launch {
            updateFavoriteCurrencyUseCase(currency)
        }
    }
}
