package ru.itis.karakurik.tinkofflab.presentation.fragments.common.recycler

import androidx.recyclerview.widget.DiffUtil
import ru.itis.karakurik.tinkofflab.domain.entity.Currency

class DiffCurrencyCallback : DiffUtil.ItemCallback<Currency>() {

    override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
        return oldItem.ticker == newItem.ticker
    }

    override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
        return oldItem == newItem
    }
}
