package ru.itis.karakurik.tinkofflab.domain.repository

import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.entity.CurrencyGraphEntity
import ru.itis.karakurik.tinkofflab.domain.entity.SupportedCurrency

interface MarketRepository {

    suspend fun getCurrenciesRateFromRemote(
        base: String,
        startDate: String?,
        endDate: String?
    ): List<Currency>

    suspend fun getCurrenciesRateFromLocal(
        base: String
    ): List<Currency>

    suspend fun convert(
        from: String,
        to: String,
        amount: Double
    ): Double?

    suspend fun getSupportedSymbols(): List<SupportedCurrency>

    suspend fun getCurrencyTimeseries(
        base: String,
        currency: String,
        startDate: String,
        endDate: String
    ): Array<CurrencyGraphEntity>?
}
