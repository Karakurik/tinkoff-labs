package ru.itis.karakurik.tinkofflab.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.itis.karakurik.tinkofflab.domain.entity.Currency

@Database(
    entities = [
        Currency::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
}