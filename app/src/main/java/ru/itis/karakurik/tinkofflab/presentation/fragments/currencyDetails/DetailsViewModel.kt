package ru.itis.karakurik.tinkofflab.presentation.fragments.currencyDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.itis.karakurik.tinkofflab.domain.entity.CurrencyGraphEntity
import ru.itis.karakurik.tinkofflab.domain.usecase.GetCurrencyTimeseriesUseCase
import ru.itis.karakurik.tinkofflab.domain.utils.State
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val getCurrencyTimeseriesUseCase: GetCurrencyTimeseriesUseCase
) : ViewModel() {

    private var _currencyTimeseries: MutableSharedFlow<State<Array<CurrencyGraphEntity>>> =
        MutableSharedFlow()
    val currencyTimeseries = _currencyTimeseries.asSharedFlow()

    fun getCurrencyTimeseries(
        base: String,
        ticker: String,
        startDate: String,
        endDate: String
    ) {
        viewModelScope.launch {
            _currencyTimeseries.emit(State.loading())
            _currencyTimeseries.emit(
                getCurrencyTimeseriesUseCase(
                    base, ticker, startDate, endDate
                )?.let {
                    State.success(it)
                } ?: State.error()
            )
        }
    }
}
