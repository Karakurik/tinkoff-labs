package ru.itis.karakurik.tinkofflab.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itis.karakurik.tinkofflab.data.database.CurrencyDao
import ru.itis.karakurik.tinkofflab.di.module.AppModule
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import javax.inject.Inject

class GetFavoritesCurrenciesUseCase @Inject constructor(
    private val currencyDao: CurrencyDao,
    @AppModule.IODispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) {

    suspend operator fun invoke(): List<Currency> {
        return withContext(dispatcher) {
            currencyDao.getAllFavouriteCurrencies()
        }
    }
}