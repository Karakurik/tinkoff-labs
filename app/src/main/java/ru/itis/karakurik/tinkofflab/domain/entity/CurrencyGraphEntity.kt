package ru.itis.karakurik.tinkofflab.domain.entity

import com.jjoe64.graphview.series.DataPointInterface
import ru.itis.karakurik.tinkofflab.domain.utils.DateHelper

data class CurrencyGraphEntity(
    val ticker: String,
    var base: String?,
    var rate: Double,
    var date: String
) : DataPointInterface {

    override fun getX(): Double {
        return DateHelper.toDouble(date)
    }

    override fun getY(): Double {
        return rate
    }
}
