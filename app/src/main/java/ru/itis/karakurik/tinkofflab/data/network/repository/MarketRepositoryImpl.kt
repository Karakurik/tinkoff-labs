package ru.itis.karakurik.tinkofflab.data.network.repository

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.itis.karakurik.tinkofflab.data.database.CurrencyDao
import ru.itis.karakurik.tinkofflab.data.network.CurrencyAppApi
import ru.itis.karakurik.tinkofflab.data.network.mapper.*
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.entity.CurrencyGraphEntity
import ru.itis.karakurik.tinkofflab.domain.entity.SupportedCurrency
import ru.itis.karakurik.tinkofflab.domain.repository.MarketRepository
import javax.inject.Inject

@BoundTo(supertype = MarketRepository::class, component = SingletonComponent::class)
class MarketRepositoryImpl @Inject constructor(
    private val currencyAppApi: CurrencyAppApi,
    private val currencyDao: CurrencyDao
) : MarketRepository {

    override suspend fun getCurrenciesRateFromRemote(
        base: String,
        startDate: String?,
        endDate: String?
    ): List<Currency> = currencyAppApi.getCurrenciesRate(base)
        .mapToCurrenciesList()
        .onEach {
            it.base = base
        }

    override suspend fun getCurrenciesRateFromLocal(
        base: String
    ) = currencyDao.getAllCurrencies()

    override suspend fun convert(from: String, to: String, amount: Double): Double? {
        return currencyAppApi.convert(from, to, amount).result
    }

    override suspend fun getSupportedSymbols(): List<SupportedCurrency> {
        return currencyAppApi.getSupportedSymbols().mapToSupportedCurrencyList()
    }

    override suspend fun getCurrencyTimeseries(
        base: String,
        currency: String,
        startDate: String,
        endDate: String
    ): Array<CurrencyGraphEntity>? = currencyAppApi.getCurrencyTimeseries(
        base, currency, startDate, endDate
    ).mapToCurrencyGraphEntityArray()
}
