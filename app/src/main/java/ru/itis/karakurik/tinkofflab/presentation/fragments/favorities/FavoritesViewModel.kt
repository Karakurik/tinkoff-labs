package ru.itis.karakurik.tinkofflab.presentation.fragments.favorities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.itis.karakurik.tinkofflab.domain.entity.Currency
import ru.itis.karakurik.tinkofflab.domain.usecase.UpdateFavoriteCurrencyUseCase
import ru.itis.karakurik.tinkofflab.domain.usecase.GetFavoritesCurrenciesUseCase
import ru.itis.karakurik.tinkofflab.domain.usecase.SaveFavoriteCurrencyUseCase
import ru.itis.karakurik.tinkofflab.domain.utils.State
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val getFavoritesCurrenciesUseCase: GetFavoritesCurrenciesUseCase,
    private val saveFavoriteCurrencyUseCase: SaveFavoriteCurrencyUseCase,
    private val updateFavoriteCurrencyUseCase: UpdateFavoriteCurrencyUseCase
) : ViewModel() {

    private var _favouriteCurrencies: MutableSharedFlow<State<List<Currency>>> =
        MutableSharedFlow()
    val favouriteCurrencies = _favouriteCurrencies.asSharedFlow()

    fun getFavouriteCurrencies() {
        viewModelScope.launch {
            _favouriteCurrencies.emit(State.loading())
            _favouriteCurrencies.emit(
                getFavoritesCurrenciesUseCase().let {
                    State.success(it)
                }
            )
        }
    }

    fun addCurrencyToFavorites(
        currency: Currency
    ) {
        viewModelScope.launch {
            saveFavoriteCurrencyUseCase(currency)
        }
    }

    fun deleteCurrencyFromFavorites(currency: Currency) {
        viewModelScope.launch {
            updateFavoriteCurrencyUseCase(currency)
        }
    }
}
