package ru.itis.karakurik.tinkofflab.di.qualifier

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class LoggingInterceptor
