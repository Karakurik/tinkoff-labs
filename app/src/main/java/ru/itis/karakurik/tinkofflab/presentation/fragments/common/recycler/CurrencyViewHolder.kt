package ru.itis.karakurik.tinkofflab.presentation.fragments.common.recycler

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import coil.load
import ru.itis.karakurik.tinkofflab.R
import ru.itis.karakurik.tinkofflab.databinding.ListItemBinding
import ru.itis.karakurik.tinkofflab.domain.entity.Currency

class CurrencyViewHolder(
    private val binding: ListItemBinding,
    private val changeFavourite: (currency: Currency) -> Unit,
    private val onItemClick: (ticker: String) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(
            parent: ViewGroup,
            changeFavourite: (currency: Currency) -> Unit,
            onItemClick: (ticker: String) -> Unit,
        ) = CurrencyViewHolder(
            ListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            changeFavourite,
            onItemClick
        )
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: Currency) {
        with(binding) {
            tvRate.text = root.resources.getString(
                R.string.rate,
                item.base,
                item.endRate,
                item.ticker
            )
            tvRateInverse.text = root.resources.getString(
                R.string.rate,
                item.ticker,
                item.endRateInverse,
                item.base
            )
            tvChangePct.text =
                (if (item.changePctInverse > 0) "+" else "") + root.resources.getString(
                    R.string.change_pct,
                    item.changePctInverse
                )

            tvChangePct.setTextColor(
                ContextCompat.getColor(
                    root.context,
                    if (item.changePctInverse > 0) R.color.plus_rate else R.color.minus_rate
                )
            )

            if (item.isFavourite) {
                ivStar.load(R.drawable.ic_star_filled_24)
            }

            root.setOnClickListener {
                onItemClick(item.ticker)
            }

            ivStar.setOnClickListener {
                if (item.isFavourite) {
                    item.isFavourite = false
                    ivStar.load(R.drawable.ic_star_outline_24)
                } else {
                    item.isFavourite = true
                    ivStar.load(R.drawable.ic_star_filled_24)
                }
                changeFavourite(item)
            }
        }
    }
}
