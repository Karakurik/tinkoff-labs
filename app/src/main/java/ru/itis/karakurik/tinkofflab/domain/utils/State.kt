package ru.itis.karakurik.tinkofflab.domain.utils

data class State<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {

        fun <T> success(data: T? = null): State<T> {
            return State(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String? = null): State<T> {
            return State(Status.ERROR, null, message)
        }

        fun <T> loading(data: T? = null): State<T> {
            return State(Status.LOADING, data, null)
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
