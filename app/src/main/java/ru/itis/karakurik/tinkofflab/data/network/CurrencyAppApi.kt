package ru.itis.karakurik.tinkofflab.data.network

import retrofit2.http.GET
import retrofit2.http.Query
import ru.itis.karakurik.tinkofflab.data.network.response.convert.ConvertResponse
import ru.itis.karakurik.tinkofflab.data.network.response.currencies.CurrenciesRateResponse
import ru.itis.karakurik.tinkofflab.data.network.response.symbols.SupportedSymbolsResponse
import ru.itis.karakurik.tinkofflab.domain.utils.DateHelper

interface CurrencyAppApi {

    @GET("/symbols")
    suspend fun getSupportedSymbols(): SupportedSymbolsResponse

    @GET("/convert")
    suspend fun convert(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount: Double
    ): ConvertResponse

    @GET("/fluctuation")
    suspend fun getCurrenciesRate(
        @Query("base") base: String? = "USD",
        @Query("start_date") startDate: String? = DateHelper.daysAgo(),
        @Query("end_date") endDate: String? = DateHelper.today()
    ): CurrenciesRateResponse

    @GET("/timeseries")
    suspend fun getCurrencyTimeseries(
        @Query("base") base: String = "USD",
        @Query("symbols") currency: String,
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String
    ): CurrenciesRateResponse
}
