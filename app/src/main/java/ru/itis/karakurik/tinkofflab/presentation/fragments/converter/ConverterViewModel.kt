package ru.itis.karakurik.tinkofflab.presentation.fragments.converter

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.itis.karakurik.tinkofflab.domain.entity.SupportedCurrency
import ru.itis.karakurik.tinkofflab.domain.usecase.ConvertUseCase
import ru.itis.karakurik.tinkofflab.domain.usecase.GetSupportedSymbolsUseCase
import ru.itis.karakurik.tinkofflab.domain.utils.State
import javax.inject.Inject

@HiltViewModel
class ConverterViewModel @Inject constructor(
    private val convertUseCase: ConvertUseCase,
    private val getSupportedSymbolsUseCase: GetSupportedSymbolsUseCase
) : ViewModel() {
    private var _convertStateFlow: MutableSharedFlow<State<Double>> = MutableSharedFlow()
    val convertStateFlow = _convertStateFlow.asSharedFlow()

    private var _supportedCurrencies =
        MutableStateFlow<State<List<SupportedCurrency>>>(State.loading(null))
    val supportedCurrencies = _supportedCurrencies.asStateFlow()

    fun getSupportedCurrencies() {
        viewModelScope.launch {
            _supportedCurrencies.value = State.loading(null)
            _supportedCurrencies.value = State.success(getSupportedSymbolsUseCase())
        }
    }

    fun convert(from: String, to: String, amount: Double) {
        viewModelScope.launch {
            _convertStateFlow.emit(State.loading(null))
            _convertStateFlow.emit(convertUseCase(from, to, amount)?.let {
                State.success(it)
            } ?: State.error("Не удалось выполнить конвертацию"))
        }
    }
}
